package bigData.wordCounter;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Mapper;

public class WordCounterMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

	private final static IntWritable ONE = new IntWritable(1);
	private Text word = new Text();

	public void map(Object key, Text value, Context context)
			throws IOException, InterruptedException {
		StringTokenizer st = new StringTokenizer(value.toString(),
				" \t\n\r\f,.:;?![]{}()'");
		while (st.hasMoreTokens()) {
			word.set(st.nextToken().toLowerCase());
			context.write(word, ONE);
		}
	}
}
