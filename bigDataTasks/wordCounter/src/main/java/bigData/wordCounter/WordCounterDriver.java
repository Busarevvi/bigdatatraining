package bigData.wordCounter;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WordCounterDriver extends Configured implements Tool {
	public static void main(String[] args) throws Exception {

		System.exit(ToolRunner.run(new Configuration(),
				new WordCounterDriver(), args));
	}

	@Override
	public int run(String[] arg0) throws Exception {
		if (arg0.length != 2) {
			throw new IllegalArgumentException(
					"Usage: WordCounterDriver <input> <output>");
		}

		Path input = new Path(arg0[0]);
		Path output = new Path(arg0[1]);

		Configuration conf = getConf();

		// FileSystem.newInstance(conf).delete(output, true);
		// can't find method "newInstance" in the class

		Job job = Job.getInstance(conf);
		job.setJarByClass(getClass());
		job.setMapperClass(WordCounterMapper.class);
		job.setReducerClass(WordCounterReducer.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.setInputPaths(job, input);
		FileOutputFormat.setOutputPath(job, output);

		return job.waitForCompletion(true) ? 0 : 1;
	}
}
