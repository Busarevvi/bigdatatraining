package bigData.invertIndex;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class InvertReduce extends Reducer<Text, Text, Text, Text> {

	public void reduce(Text key, Iterable<Text> values,
			Context context)
			throws IOException, InterruptedException {
		
		StringBuilder toReturn = new StringBuilder();
		for (Text txt : values) {
			toReturn.append(txt+" ");
		}
		context.write(key, new Text(toReturn.toString()));
	}
}
