package bigData.invertIndex;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
//import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class InvertDriver extends Configured implements Tool {
	public static void main(String[] args) throws Exception {

		System.exit(ToolRunner.run(new Configuration(), new InvertDriver(),
				args));
	}

	@Override
	public int run(String[] args) throws Exception {
		if (args.length != 2) {
			throw new IllegalArgumentException(
					"Usage: InvertDriver <input> <output>");
		}

		Path input = new Path(args[0]);
		Path output = new Path(args[1]);

		Configuration conf = getConf();

		// FileSystem.newInstance(conf).delete(output, true);
		// can't find method "newInstance" in the class

		Job job = Job.getInstance(conf);
		job.setJarByClass(getClass());
		job.setMapperClass(InvertMapper.class);
		job.setReducerClass(InvertReduce.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		FileInputFormat.setInputPaths(job, input);
		FileOutputFormat.setOutputPath(job, output);

		return job.waitForCompletion(true) ? 0 : 1;

	}

}
